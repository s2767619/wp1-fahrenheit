package nl.utwente.di.bookQuote;

import java.util.HashMap;
import java.util.Map;

public class Quoter {

    public double celsiusToFahrenheit(String s) {
        return Double.parseDouble(s) * 9/5 + 32;
    }
}
